TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += qt console warn_on depend_includepath testcase
QT += widgets testlib

SOURCES += \
        exceptions.h \
        iterator.h \
        vector.h

# install
target.path = $$[QT_INSTALL_EXAMPLES]/qtestlib/tutorial1
INSTALLS += target
