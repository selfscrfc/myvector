#ifndef VECTOR_EXCEPTIONS_H
#define VECTOR_EXCEPTIONS_H
#include <iostream>
#include <utility>

class Tests;

class Exceptions {
private:
    std::string text;
public:
    explicit Exceptions(std::string str) {text = std::move(str);}
    friend class Tests;
};

#endif //VECTOR_EXCEPTIONS_H
